export class Comida{
    private nome: string;
    private calorias: number;
    private proteina: number;
    private gordura: number;
    private glutem: boolean;
    private static QntComida: number = 0;


    constructor(nome:string, calorias:number, proteina: number, gordura: number, glutem:boolean){
        this.nome = nome;
        this.calorias = calorias;
        this.proteina = proteina;
        this.gordura = gordura;
        this.glutem = glutem;
        Comida.QntComida ++;
    }

    public getCalorias(): number{
        return this.calorias;
    }
}