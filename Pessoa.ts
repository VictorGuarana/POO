import { Comida } from './Comida';

export class Pessoa{
    private nome: string;
    private calorias_diarias_totais: number;
    private calorias_diarias_disponiveis: number;
    private static qntPessoas: number = 0;

    constructor(nome:string, calorias_diarias_totais: number, calorias_diarias_disponiveis:number){
        this.nome = nome;
        this.calorias_diarias_totais = calorias_diarias_totais;
        this.calorias_diarias_disponiveis = calorias_diarias_disponiveis;
        Pessoa.qntPessoas ++;

    }

    public static getQntPessoas(): number{
        return Pessoa.qntPessoas;
    }

    public come(comida:Comida): void{
        if (this.calorias_diarias_disponiveis >= comida.getCalorias())
        this.calorias_diarias_disponiveis -= comida.getCalorias();
    }

    public outro_dia():void{
        this.calorias_diarias_disponiveis = this.calorias_diarias_totais;
    }
}
